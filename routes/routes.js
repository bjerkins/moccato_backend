'use strict';

var Validator = require('../service/validator.js');

/**
 * Expose routes
 */

module.exports = function (router) {
  
  // test route to make sure everything is working (accessed at GET http://localhost:8080/api)
  router.get('/', function(req, res) {
    res.json({ message: 'Hai! Welcome to Moccato Backend api!' });
  });

  router.post('/token', function (req, res) {
    var token = req.body.token;
    if (typeof token === 'undefined') {
      res.status(400).send('Missing token');
    } else {
      var check = Validator.validate(token);
      if (check === null) {
        res.status(418).send('Invalid token');
      } else {
        res.json({message: 'Token validated with rounded up (negative) minutes since: ' + check.delta});
      }
    }
  });

};