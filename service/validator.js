'use strict';

var notp        = require('notp'),
    key         = require('../config/config').appKey();
    // thirtyTwo   = require('thirty-two'),
    // base32      = require('base32');

/*
 * Function that checks whether a given token is valid or not
 * param: token - the OTP token
 */
module.exports = {
  validate: function (token) {
    if (typeof token === 'undefined' || token === '') {
      return false;
    }

    console.log('checking token: ', token, ' with key: ', key);
    return notp.totp.verify(token, key);
    
    // if (check === null) {
    //   return false;
    // } else {
    //   return check.delta;
    // }
    // if (check === null) {
    //   return false;
    // } else {
    //   return check.delta === 0;
    // }

  }
};