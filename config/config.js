'use strict';

var fs = require('fs');

// here by configs!
module.exports = {
  appKey: function () {
    return JSON.parse(fs.readFileSync('./config/config.json', 'utf8')).key;
  }
};