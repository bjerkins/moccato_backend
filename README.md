# Moccato Backend API
This repository consists of the backend server API for the Moccato iOS application.

### Version
1.0

### Installation
The following will install all the dependencies and start the server on localhost:8080

```sh
$ npm install
$ node server.js
```

### Devloping
Running

```sh
$ grunt build
```

will automatically watch changes made by you, run the jshint task and run all the unit tests. Finally, it will update the server with the latest changes.
