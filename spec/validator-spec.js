
'use strict';

describe('Validation', function () {

  var mockery   = require('mockery'),
      validator;

  var notpMock = {
    totp: {
      verify:  function () {
        return {
          delta: 0
        };
      }
    },
    hotp: {
      verify: function () {
        return true;
      }
    }
  };

  beforeEach(function () {

    mockery.enable({
      warnOnUnregistered: false
    });
    mockery.registerMock('notp', notpMock);
    mockery.registerAllowable('../service/validator.js');

    validator = require('../service/validator.js');
  });

  afterEach(function () {
    mockery.deregisterAll();
    mockery.disable();
  });
  
  describe('initialization', function () {
    it('should have a \'validate\' function', function () {
      expect(validator.validate).toBeDefined();
    });
  });

  // describe('the service', function () {
  //   var spy = null;

  //   beforeEach(function () {
  //     spy = spyOn(notpMock.totp, 'verify').and.callThrough();
  //   });

  //   it('should return false when called with no token', function () {
  //     expect(validator.validate('')).toBe(false);
  //     expect(validator.validate()).toBe(false);
  //   });
    
  //   it('should try to verify tokens using library', function () {
  //     validator.validate('asdf');
  //     expect(spy).toHaveBeenCalled();
  //   });
  // });
});