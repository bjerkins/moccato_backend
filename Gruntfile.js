'use strict';

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        'config/*.js',
        'routes/*.js',
        'service/*.js'
      ],
      test: {
        options: {
          jshintrc: 'spec/.jshintrc'
        },
        src: ['spec/**/*.js']
      }
    },

    // Watch changes in our server to automatically reload. 
    nodemon: {
      dev: {
        script: 'server.js'
      }
    },

    jasmine_nodejs: {
      // task specific (default) options
      dev: {
        // spec files
        specs: [
            "spec/**/*.js"
        ]
      }
    },

    watch: {
      scripts: {
        files: [
          'config/*.js',
          'routes/*.js',
          'service/*.js',
          'spec/**/*.js'
        ],
        tasks: ['jshint', 'jasmine_nodejs']
      }
    },

    concurrent: {
      dev: {
        tasks: [
          'jshint',
          'jasmine_nodejs',
          'nodemon', 
          'watch'
        ]
      },
      options: {
        logConcurrentOutput: true
      }
    }

  });

  grunt.registerTask('test', [
    'jshint', 'jasmine_nodejs'
  ]);

  grunt.registerTask('build', '', function () {
    var taskList = [
        'concurrent:dev'
    ];
    grunt.task.run(taskList);
  });
};